package view;

import controller.DataGetter;

/**
 * Charts interface.
 *
 */
public interface Charts {

    /**
     * Method for updating the charts.
     * 
     * @param updater
     */
    void update(DataGetter updater);
}
